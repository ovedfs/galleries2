<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
    	return view('gallery.create');
    }

    public function store(Request $request)
    {
    	//dd($request->all());
    	$request->validate([
    		'name' => 'required',
    		'description' => 'required'
    	]);

    	$gallery = new Gallery;
    	$gallery->name = $request->name;
    	$gallery->description = $request->description;
    	$gallery->user_id = auth()->user()->id;

    	if($gallery->save()){
    		return redirect()->route('home')
    			->with('msg', 'Galería creada satisfactoriamente!');
    	}

    	return redirect()->route('home')
    		->with('msg', 'La Galería NO pudo ser creada!');
    }

    public function photos(Gallery $gallery)
    {
    	return view('gallery.photos', compact('gallery'));
    }
}
