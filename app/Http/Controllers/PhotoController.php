<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;
use App\Photo;

class PhotoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(Gallery $gallery)
    {
    	return view('photo.create', compact('gallery'));
    }

    public function store(Request $request, Gallery $gallery)
    {
    	//dd($request->all());

    	$request->validate([
    		'caption' => 'required',
    		'image' => 'required|image|mimes:jpeg,jpg,png,gif',
    	]);

    	$file = $request->file('image');
    	$filename = 'photo-' . time() . '.' . $file->getClientOriginalExtension();
    	$file->storeAs('public', $filename);

    	$photo = new Photo;
    	$photo->image = $filename;
    	$photo->caption = $request->caption;
    	$photo->gallery_id = $gallery->id;

    	if($photo->save()){
    		return redirect()->route('galleries.photos', $gallery)
    			->with('msg', 'Foto agregada correctamente!');
    	}

    	return redirect()->route('galleries.photos', $gallery)
    		->with('msg', 'La Foto NO pudo ser agregada!');
    }
}
