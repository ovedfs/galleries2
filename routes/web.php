<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('galleries/create', 'GalleryController@create')->name('galleries.create');
Route::post('galleries/store', 'GalleryController@store')->name('galleries.store');
Route::get('galleries/{gallery}/photos', 'GalleryController@photos')->name('galleries.photos');

Route::get('photos/{gallery}/create', 'PhotoController@create')->name('photos.create');
Route::post('photos/{gallery}/store', 'PhotoController@store')->name('photos.store');