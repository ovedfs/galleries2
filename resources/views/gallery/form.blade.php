@include('partials.errors')

{{ Form::open(['route' => 'galleries.store']) }}

	<div class="form-group">
		{{ 
			Form::text('name', null, [
				'class' => 'form-control', 
				'placeholder' => 'Ingresa el nombre...'
			]) 
		}}
	</div>

	<div class="form-group">
		{{ 
			Form::textarea('description', null, [
				'class' => 'form-control', 
				'placeholder' => 'Ingresa la descripción...',
				'rows' => 3
			]) 
		}}
	</div>

	<div class="form-group">
		<a href="{{ route('home') }}" class="btn btn-secondary">
			Regresar
		</a>

		<button type="submit" class="btn btn-primary">
			Guardar
		</button>
	</div>
	

{{ Form::close() }}
