@extends('layouts.app')

@section('content')
<div class="container">
    <h1>MIS GALERÍAS <small>[Nueva Galería]</small></h1><hr>

    <div class="row justify-content-md-center">
    	<div class="col-md-6">
    		<div class="card bg-light">
    			<div class="card-header">Nueva Galería de {{ auth()->user()->name }}</div>
    			<div class="card-body">
    				@include('gallery.form')
    			</div>
    		</div>
    	</div>
    </div>
</div>
@endsection
