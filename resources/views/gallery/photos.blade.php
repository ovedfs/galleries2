@extends('layouts.app')

@section('content')
<div class="container text-center">
    <h1>{{ $gallery->name }} <a href="{{ route('photos.create', $gallery) }}" class="btn btn-primary">+ Foto</a></h1>
    <h2>{{ $gallery->description }}</h2><hr>

    @if($gallery->photos->count())
        <div class="card-columns">
            @foreach($gallery->photos as $photo)
                @include('photo.card')
            @endforeach
        </div>
    @else
        <p>No tiene fotos</p>
    @endif
</div>
@endsection
