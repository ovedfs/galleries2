<div class="card bg-light">
	<div class="card-header">
		{{ $gallery->name }}
	</div>
	<div class="card-body">
		{{ $gallery->description }}
	</div>
	<div class="card-footer text-center">
		<a href="{{ route('galleries.photos', $gallery) }}" class="btn btn-primary">
			Ver fotos
		</a>
	</div>
</div>