@extends('layouts.app')

@section('content')
<div class="container text-center">
    <h1>{{ $gallery->name }} <small>[Nueva foto]</small></h1>
    <h2>{{ $gallery->description }}</h2><hr>

    <div class="row justify-content-md-center">
    	<div class="col-md-6">
    		<div class="card bg-light">
    			<div class="card-header">Nueva Foto</div>
    			<div class="card-body">
    				@include('photo.form')
    			</div>
    		</div>
    	</div>
    </div>
</div>
@endsection
