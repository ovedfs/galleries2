@include('partials.errors')

{{ Form::open(['route' => ['photos.store', $gallery], 'files' => true]) }}

	<div class="form-group">
		{{ 
			Form::text('caption', null, [
				'class' => 'form-control', 
				'placeholder' => 'Ingresa el caption...'
			]) 
		}}
	</div>

	<div class="form-group">
		{{ Form::file('image') }}
	</div>

	<div class="form-group">
		<a href="{{ route('galleries.photos', $gallery) }}" class="btn btn-secondary">
			Regresar
		</a>

		<button type="submit" class="btn btn-primary">
			Guardar
		</button>
	</div>
	

{{ Form::close() }}
