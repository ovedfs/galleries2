<div class="card" style="width: 18rem;">
	<img class="card-img-top" src="{{ asset('storage/' . $photo->image) }}" alt="Card image cap">
	<div class="card-body">
		<h5 class="card-title">{{ $photo->caption }}</h5>
	</div>
	<div class="card-footer">
		<a href="#" class="btn btn-danger">Eliminar</a>
	</div>
</div>