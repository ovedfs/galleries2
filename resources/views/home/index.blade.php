@extends('layouts.app')

@section('content')
<div class="container">
    <h1>MIS GALERÍAS <a href="{{ route('galleries.create') }}" class="btn btn-primary">+ Galería</a></h1><hr>

    @if($user->galleries->count())
        <div class="card-columns">
        	@foreach($user->galleries as $gallery)
				@include('gallery.card')
        	@endforeach
        </div>
    @else
        <p>No tiene galerias...</p>
    @endif
</div>
@endsection
